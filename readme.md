# Robot Path Finder
## Installation Instructions:
Requires Python 2.7+ and pip. Run `$ pip install -r requirements.txt` to install requirements.
## Running Instructions:
Run `$ python main.py <input file>`. The input file must contain on each line an equal number of space separated hex values. The hex digits can be either upper or lower case.
## Assumptions Made:
1. The path is being made from node to node.
2. The hex values represent node weights.
3. The shortest path is the one that minimises the sum of the weights of all nodes visited.
4. The graph must be of equal width and height.
5. There are no blank nodes (weightless nodes should be 0).
6. The program will not ignore nodes that cannot be converted, but will alert the user.
7. The program will not try to interpret any unusual input, but will alert the user of bad input and stop execution.
8. The output will be printed to the console that runs it. If you want to save the output, run `$ python main.py <input file> > <outputfilename>` to store the output in that file.
