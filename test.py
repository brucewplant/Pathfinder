import unittest
import main


class NodeCoordinateTest(unittest.TestCase):
    def testPerfectCase(self):
        self.assertEqual(main.get_node_coordinates('1:2'), (1, 2))

    def testNotAnInteger(self):
        with self.assertRaises(ValueError):
            self.assertRaises(ValueError, main.get_node_coordinates('a:b'))


class NodeNameTest(unittest.TestCase):
    def testPerfectCase(self):
        self.assertEqual(main.get_node_name((1, 2)), '1:2')


class PathDirectionTest(unittest.TestCase):
    def testStraightPath(self):
        testPath = ['0:0', '0:1', '0:2', '0:3']
        exampleOutput = ['r', 'r', 'r']
        self.assertEqual(main.get_path_directions(testPath), exampleOutput)

    def testWindyPath(self):
        testPath = ['0:0', '1:0', '1:1', '0:1', '0:2', '1:2']
        exampleOutput = ['d', 'r', 'u', 'r', 'd']
        self.assertEqual(main.get_path_directions(testPath), exampleOutput)


if __name__ == '__main__':
    unittest.main()
