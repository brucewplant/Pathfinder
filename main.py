import networkx
import sys
import os


def get_node_name(coordinate):
    # Single function to generate node name from indicies. Ensures we always
    # use the same formatting.
    return '%d:%d' % (coordinate[0], coordinate[1])


def get_node_coordinates(name):
    splitname = name.split(':')

    return tuple([int(i) for i in splitname])


def get_path_directions(path):
    tupleized_path = [get_node_coordinates(i) for i in path]
    output = []
    for i in xrange(1, len(tupleized_path)):
        if tupleized_path[i-1][0] == tupleized_path[i][0]:
            if tupleized_path[i-1][1] < tupleized_path[i][1]:
                output += 'r'
            else:
                output += 'l'
        else:
            if tupleized_path[i-1][0] < tupleized_path[i][0]:
                output += 'd'
            else:
                output += 'u'

    return output


def shortest_path(graph, start, finish):
    return networkx.shortest_path(
        graph,
        source=get_node_name(start),
        target=get_node_name(finish),
        weight='weight'
    )


def generate_graph(input_array):
    # Receives a 2d array of integers converts to a graph of weighted edges
    graph = networkx.DiGraph()

    for i in xrange(len(input_array)):
        for j in xrange(len(input_array[i])):
            current_node_name = get_node_name((i, j))
            graph.add_node(current_node_name, weight=input_array[i][j])
            # Add edges going into this node and coming out
            if i > 0:
                left_node = get_node_name((i - 1, j))
                graph.add_edge(
                    left_node, current_node_name,
                    weight=input_array[i][j]
                )
                graph.add_edge(
                    current_node_name, left_node,
                    weight=graph.node[left_node]['weight']
                )
            if j > 0:
                upper_node = get_node_name((i, j - 1))
                graph.add_edge(
                    upper_node, current_node_name,
                    weight=input_array[i][j]
                )
                graph.add_edge(
                    current_node_name, upper_node,
                    weight=graph.node[upper_node]['weight']
                )

    return graph


def convert_input(input_string):
    # converts input file to an array of positive integers
    lines = []
    output = []

    for line in input_string:
        output_row = []
        hex_items = line.split(' ')

        for hex_item in hex_items:
            try:
                output_row.append(int(hex_item, 16))
            except ValueError:
                raise ValueError('%s is not a hex number!' % (hex_item))
        output.append(output_row)

    return output


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'You must specify an input file.'
        exit()

    starting_list = []
    try:
        with open(sys.argv[1]) as input_file:
            for line in input_file:
                hex_items = line.strip().split(' ')
                starting_list += [[int(i, 16) for i in hex_items]]
    except IOError:
        print 'The file you specified does not exist.'
        exit()
    except ValueError as e:
        print 'There was a problem with the file you specified: %s' % (e)
        exit()
    except Exception as e:
        print e
        print 'Something went wrong, please contact the developer for help.'
        exit()

    graph = generate_graph(starting_list)
    last_index = (len(starting_list) - 1, len(starting_list[0]) - 1)
    path = shortest_path(graph, (0, 0), last_index)
    print (' ').join(get_path_directions(path))
